#!/usr/bin/bash

case $1 in 
    l) bspc query -N -n .hidden ;; 
    s) bspc node $(bspc query -N -n .hidden | tail -1) --flag hidden ;;
    h) bspc node --flag hidden ;;
    *) echo -e "l (list)\ns (show)\nh (hide active)\n"; exit 1 ;;
esac
exit 0

# TODO
# needs testing
