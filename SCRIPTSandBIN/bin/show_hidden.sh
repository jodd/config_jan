#!/usr/bin/sh

# shows all nodes hidden

for i in $(bspc query -N -n .hidden); do
  bspc node $i --flag hidden
done
