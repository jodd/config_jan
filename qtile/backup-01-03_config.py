# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# WorkInProgress by jan 2021

import os
# import re
# import socket
import subprocess
##
from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
##
from libqtile import hook

##
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~/.config/qtile/scripts/autostart.sh')
    subprocess.call([home])

mod = "mod4"
alt_key = 'mod1'
shift_key = 'shift'
ctrl_key = 'control'

terminal = guess_terminal()

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "d", lazy.spawn('rofi -show run')),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod], "Left", lazy.screen.prev_group(skip_empty=True)),
    Key([mod], "Right", lazy.screen.next_group(skip_empty=True)),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "i", lazy.spawn('/usr/bin/ranger'), desc="Ranger file manager"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "space", lazy.spawn('ranger'), desc="File manager"),

    Key([mod, shift_key], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, shift_key], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, shift_key], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, shift_key], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    Key([mod, shift_key], "Left", lazy.screen.toggle_group()),
    Key([mod, shift_key], "Return", lazy.spawn('urxvt'), desc="Launch urxvt"),

    Key([mod, ctrl_key], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, ctrl_key], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, ctrl_key], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, ctrl_key], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod, ctrl_key], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, ctrl_key], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod, ctrl_key], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    
    Key([mod, alt_key], "f", lazy.spawn('firefox'), desc="Browser firefox"),
    Key([mod, alt_key], "q", lazy.spawn('qutebrowser'), desc="Browser qutebrowser"),

    
    # Switch between windows

    # Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes


]

##
groups = [Group(i) for i in "1234567890"]

for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)),
        Key([mod, shift_key], i.name, lazy.window.togroup(i.name, switch_group=True), desc="Switch to & move focused window to group {}".format(i.name)),
    ])

layouts = [
    layout.Bsp(),
    layout.Max(),
]
    # layouts above alternate with mW + tab
    # layout.Floating(),
    # layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], border_width=4),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Slice(),
    # layout.Stack(num_stacks=2),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),

widget_defaults = dict(font='sans', fontsize=12, padding=5,)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentLayout(),
                widget.GroupBox(),
                widget.Spacer(),
                widget.Chord(chords_colors={'launch': ("#ff0000", "#ffffff"),},name_transform=lambda name: name.upper(),),
                widget.Prompt(),
                widget.Systray(),
                widget.Spacer(),
                widget.Clock(format='%Y-%m-%d %a %H:%M '),
                # widget.WindowName(),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # widget.QuickExit(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.


wmname = "LG3D"

