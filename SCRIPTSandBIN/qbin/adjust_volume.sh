#!/usr/bin/bash

# usage:  adjust_volume.sh {toggle,mute,up [1-100],down [1-100]}

NUM="${2:-5}"

case $(echo "$1" | cut -c1) in
    t)  amixer sset Master toggle ;;
    m)  amixer sset Master mute ;;
    u)  amixer sset Master "$NUM"%+ ;;
    d)  amixer sset Master "$NUM"%- ;;
esac >/dev/null 

# refresh polybar is done by the alsa plugin?
