#!/usr/bin/sh

start_galculator() {
    pidof galculator && killall galculator
    [ -f /tmp/node_id_galculator ] && rm /tmp/node_id_galculator
    /usr/bin/galculator&
    sleep 3
    xwininfo -root -tree | grep "Galculator\")" | tail -1 | cut -c6-14 > /tmp/node_id_galculator
    exit 0
}

pidof galculator>/dev/null || start_galculator

pidof galculator && \
    bspc node $(< /tmp/node_id_galculator) --flag hidden && \
    bspc node $(< /tmp/node_id_galculator) --focus
sleep 3
# added sleep, maybe it will prevent some random errors happening:
# sometimes a random node is hidden...

