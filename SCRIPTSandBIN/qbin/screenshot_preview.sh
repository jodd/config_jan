!/usr/bin/sh 

# scrot makes a screenshot
# --silent, -z
## --delay NUM, -d NUM	NUM=sekunder
## --exec APP, -e APP	-e feh åpner screenshot med feh
## --select, -s 		interaktiv
## %s is unix time
f_name="snap_"$(date +%s%N)".png"
d_name="$HOME"/Pictures/screenshots/

# /usr/bin/scrot "$HOME"/Pictures/screenshots/scrshot_%s.png
/usr/bin/scrot "$d_name$f_name"

urxvt -name "SXIV" -e sxiv -f "$d_name$f_name"

pgrep -x dunst && notify-send "$d_name$f_name"

# display /home/jan/Pictures/screenshots/scrshot_$dato.png \ 
# /usr/bin/alacritty --class "PREVIEW" \
#   --title "SCREENSHOT PREVIEW" \
#   --working-directory "$HOME"/Pictures/screenshots \
#   --command '/usr/bin/sxiv -t'


