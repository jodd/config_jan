#!/usr/bin/bash
#   Denne backup ble gjort i begynnelsen av Mai 2020
#   Er i ferd med og slå av server og ville bare ha en bakup som var
#   up-to-date



# logfil i ~
LOG="/home/jan/.backup-log"
# Year-Month-Date
DAT=$(date +%Y-%m-%d)
# Hour:Minutes:Secounds
TID=$(date +%T)
echo "*************************************************"


# Server og PATH til backup dir...
BACKUP="jan@mink:/home/jan/TRETERRAID/Backup_2020"


# echo "/etc" >> $LOG
# rsync -ravul -e ssh /etc \
# --dry-run \
rsync --recursive \
    --archive \
    --verbose \
    --update \
    --checksum \
    --safe-links \
    --rsh=ssh /home/jan/.config \
    --exclude 'chromium' \
    --exclude 'gitconfig' \
    --exclude 'google-chrome' \
    --exclude 'htop' \
    --exclude 'mozilla' \
    --exclude 'qutebrowser' \
    --exclude 'thunderbird' \
    --exclude 'BraveSoftware' \
    $BACKUP

rsync --recursive \
    --archive \
    --verbose \
    --update \
    --checksum \
    --safe-links \
    --rsh=ssh /home/jan/.ssh \
    $BACKUP

rsync --recursive \
    --archive \
    --verbose \
    --update \
    --checksum \
    --safe-links \
    --rsh=ssh /home/scripts \
    $BACKUP

rsync --recursive \
    --archive \
    --verbose \
    --update \
    --checksum \
    --safe-links \
    --rsh=ssh /home/Documents \
    $BACKUP

