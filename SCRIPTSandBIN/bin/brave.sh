#!/usr/bin/sh


brave_pid=$(pidof brave)
if [ -z "$brave_pid" ]; then
  bspc desktop 2 --focus
  /usr/bin/brave &
  exit
else
  bspc desktop 2 --focus
  exit 0
fi
