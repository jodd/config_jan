
# statusline
# https://learnvimscriptthehardway.stevelosh.com/chapters/17.html
# https://shapeshed.com/vim-statuslines/#showing-the-statusline
# http://vimdoc.sourceforge.net/htmldoc/options.html#'statusline'
function! GitBranch()
  return system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endfunction

function! StatuslineGit()
  let l:branchname = GitBranch()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':'
endfunction

# example from above www:
set statusline=
set statusline+=%#PmenuSel#
set statusline+=%{StatuslineGit()}
set statusline+=%#LineNr#
set statusline+=\ %f
set statusline+=%m\
set statusline+=%=
set statusline+=%#CursorColumn#
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=\[%{&fileformat}\]
set statusline+=\ %p%%
set statusline+=\ %l:%c
set statusline+=\ 

" My take:
set statusline=
"set statusline+=%#LineNr#
set statusline+=%#DiffDelete#
set statusline+=\ %f
set statusline+=%=
set statusline+=%m\
set statusline+=\ %y
set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
set statusline+=%=
set statusline+=%#SpellCap#
set statusline+=\ [%5l]/
set statusline+=\[%5c]
set statusline+=\ %5L
set statusline+=\ %3p%%
set statusline+=\ 

#
%#LineNr#
Bestemmer farge utfra: cmd :so $VIMRUNTIME/syntax/hitest.vim
%f ~/Documents
%F /home/jan/Documents
%m ??? vet ikke, men: %m\ viser \
%y type of file in buffer

# oneliner:
set statusline=%#DiffDelete# %f%=%y%{&fileencoding?&fileencoding:&encoding}%=%#SpellCap#[%5l]/[%5c]%5L%3p%%

    
