# = vimdiff =
::
nvim -d file1 file2
vimdiff file1 file2

vimdiff -o file1 file2
  ctrl+w h|j|k|l

do   get changes from other window
dp   put changes from other window
]c    jumt to next
]c    jump to prev
zo    open fold
zc    close
zr    reduce folding level
zm    one more folding level, please
:diffu
:diffupdate

:difffg RE  get from REMOTE
:difffg BA  get from BASE
:difffg LO  get from LOCAL

#sort in vim, line 1 - 95
:1,95sort
#sort uniqe
:1,95sort -u

