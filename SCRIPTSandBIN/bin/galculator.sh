#!/usr/bin/sh

node_id_galculator=$(xwininfo -root -tree | awk '/galculator/ { a = $1 } END { print a}')


if [ -n "$node_id_galculator" ]; then
  bspc node $node_id_galculator --flag hidden --focus
else
  /usr/bin/galculator &
fi
