#!/usr/bin/bash


pkill -f /dev/video || \
    mpv \
    --no-osc \
    --no-input-default-bindings \
    --input-conf=/dev/null \
    --autofit=30%  /dev/video0
