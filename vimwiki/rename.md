
rename -n 's/\+//g' *
The -n option shows files that will be renamed. Substitute -v for -n to actually rename the files.

rename 's/ //' *.*		#Remove blanks
rename 'y/A-Z/a-z/'		#Make A to a...
rename -n 's/\+//g' *		#-n shows file, change to -v for action
