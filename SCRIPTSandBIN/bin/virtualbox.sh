#!/usr/bin/sh


# start just one

running_virtualbox=$(pidof VirtualBox)

bspc desktop --focus 8
if [ -z $running_virtualbox ]; then
  /usr/bin/virtualbox
else
  printf "virtualbox already running!\n"
fi
