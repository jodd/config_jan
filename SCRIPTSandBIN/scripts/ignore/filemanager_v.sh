#!/usr/bin/sh


term="/usr/bin/alacritty"
desktop_nr="9"
desktop_active=$(bspc query -D -d --names)

    
if [ $desktop_nr = $desktop_active ] ;
then
# seems we are on desktop of filemanager, lets go back
    bspc desktop --focus last
    exit 0
fi
name="VIFM"
runf="$XDG_CONFIG_HOME/vifm/scripts/vifmrun"               

# OK, we know which filemanager to use, let's check if they're running and go there:
xwininfo -root -tree | grep "(\"$name" 1>/dev/null && bspc desktop --focus $desktop_nr && exit 0

# Well, since it's not running, let's start it and go there:
bspc desktop --focus $desktop_nr && \
        $term --title $name \
        --class $name \
        --command $runf
