#!/usr/bin/bash

# NB Autstarting with .xinitrc
MINK=10.0.0.89
OK=0
COUNT=0
TRETERRAID="/hdd/mink/TRETERRAID"

## if running nothing, else start service
systemctl status sshd | grep "active (running)" || sudo systemctl start sshd

# if mounted...it exitsts
if [ ! -d $TRETERRAID ]; then
	until [ $OK -ge 1 ]; do
		let OK=$( ping -c 1 $MINK | grep "1 received" | wc -l )
		let COUNT=$COUNT+1
		sleep 1
		if [ $COUNT -eq 9 ]; then
			let OK=$COUNT
		fi
	done
fi

if [ $OK -eq 1 ]; then
		sshfs \
			-o IdentityFile=/home/jan/.ssh/id_ed25519\
			-o idmap=file \
			-o uidfile=/home/jan/.local/bin/.uid_sshfs_file \
			-o gidfile=/home/jan/.local/bin/.gid_sshfs_file \
			-o nomap=ignore \
			jan@mink:/home/jan /home/jan/.ulocal/mink
fi
# -o IdentityFile=/home/jan/.ssh/id_ecdsa \
# echo "jan:1001" > /home/jan/.bin/.uid_sshfs_file
# echo "jan:1001" > /home/jan/.bin/.gid_sshfs_file
# echo "users:1001" > /home/jan/bin/uid_file
# echo "jan:1001" > /home/jan/bin/gid_file
