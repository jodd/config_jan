#!/usr/bin/sh 
# setxkbmap -model pc105 -layout no

# when keyboard was connected to usb-hub that fell out from time to time
# I had to reload norvegian keymap, crashed back to default us

layout=$(setxkbmap -query | tail -1 | cut -d' ' -f6)

if [ "$layout" = "no" ]; then
    pgrep -x dunst && notify-send -u critical "KEYBOARD" "US Layout"
    setxkbmap 'us'
elif [ "$layout" = "us" ]; then
    pgrep -x dunst && notify-send "KEYBOARD" "NO Layout"
    setxkbmap -model pc105 -layout no
fi


