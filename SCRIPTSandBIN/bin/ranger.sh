#!/usr/bin/sh

# this is a ranger starter.
# ranger is living on desktop 9
# ranger_desktop="9"

#ranger_desktop=$(awk '/ranger/ {print $5} /home/jan/.config/bspwm/bspwmrc | cut -d'=' -f2)
# ranger_desktop=$(bspc rule --list | awk '-F[ =]' '/ranger/ {print $5}')
ranger_desktop=9



# identify active desktop.
desktop_active=$(bspc query -D -d --names)





# if desktop_active is identical with ranger_desktop, then we return to previous
if [ "$desktop_active" = "$ranger_desktop" ]; then
  bspc desktop --focus last
  exit 0
fi


# the above, might cause stupid result if trying to start ranger from desktop 9
# result will just kick you back to previous.
# but however...just start ranger again, and it will act accordingly to wishes.




# since we are not yet exit'ed, we can check if ranger is running.
program_node=$(xwininfo -root -tree | awk '/RANGER/ {print $1}')



# we can now to one thing if is running, another if it's not.
# the action that takes the longest time, is in the else part.
# that way everything is fast, exept when actually starting ranger

if [ -n "$program_node" ]; then
  bspc desktop --focus $ranger_desktop
  exit 0
else
  # seems it is time to actually start ranger. first some definitions:
  terminal="/usr/bin/alacritty"
  runfile="/usr/bin/ranger"
  node_name="RANGER"
  node_class="RANGER"

  bspc desktop --focus "$ranger_desktop" && \
    $terminal --title \
    $node_name --class \
    $node_class --command $runfile
  exit 0
fi
  # bspc desktop --focus "$ranger_desktop" && \
  #   $terminal --title $node_name \
  #   --class $node_class \
  #   --command $runfile &
      # ranger is started with title (name) and class for terminal
