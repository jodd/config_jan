#!/usr/bin/sh

firefox_pid=$(pidof firefox)
if [ -z "$firefox_pid" ]; then
  bspc desktop 3 --focus
  /usr/bin/firefox &
  exit
else
  bspc desktop 3 --focus
  exit 0
fi
