# neovim

Config with plugin vim.plug

XDG_DATA_HOME

HOME

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

if the plug.vim is installed, nvim should start without error messages.

To install plugins in the list
:PlugUpdate


