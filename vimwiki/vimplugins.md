#   vim plugins

## Usage:
[[goyo]]              :Goyo / :Goyo!
[[vimcommentary]]     gcc   vimcommentary this line; 20 gcc ...
[[vimcsscolor]]       ,c    ColorHighlight  /   ,C  ColorClear
[[vimsurround]]       "vim-surround
                  "   ds"      remove    "test"  test
                  "   cs]{    makes   [test]  to:   { test }  
                  "   cs]}    makes   [test]  to:   {test}
                  "   cs]'    makes   [test]  to:   'test'
                  "   cs])    makes   [test]  to:   (test)
                  "   cs](    makes   [test]  to:   ( test )
                  "   cs]"    makes   [test]  to:   "test"
                  "   cs]'    makes   [test]  to:   'test'
                  "   cs]=    makes   [test]  to:   =test=

                  ,ww   vimwiki


# How to install

mkdir -p ~/.local/share/nvim/site/autoload
cd .local/shar/nvim/site/autoload

either:

* curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -o plug.vim
* wget https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
* git clone https://github.com/junegunn/plug-vim.git (stupid, needs to tidy up a lot of files and move plug.vim up to autoload.


To install plugins in the listing->     :PlugInstall vim-css-color
Check status->                          :PlugStatus
Remove->                                :PlugClean  #removes unlisted
Update->                                :PlugUpdate

== plugins ==

[[vimcsscolor]]
[[vimwiki]]
[[vimsurround]]
[[vimcommentary]]
[[goyo]]

will automatically find right download if existing on github (and gitlab?)



## Old notes

https://github.com/junegunn/vim-plug
neovim:
    curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    this is autoloaded by nvim
~/.config/nvim/init.vim:
    call Plug#begin('~/.local/share/plugvims')  #where to save stuff
    Plug 'ap/vim-css-color'                     #github.com=standard place to search
    call Plug#end()
    
