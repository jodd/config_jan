#!/bin/bash
XDG_CONFIG_HOME="$HOME/.config"
export XDG_CONFIG_HOME

# force ranger to read ~/.config/ranger/rc.conf and not load twice
export RANGER_LOAD_DEFAULT_RC=false

nitrogen --restore --set-zoom-fill &
picom &
/usr/bin/setxkbmap -model pc105 -layout no



