#!/usr/bin/sh

# this is a vifm starter.
# vifm is probably staying at desktop 9


vifm_desktop=$(bspc rule --list | awk '-F[ =]' '/vifm/ {print $5}')


# identify active desktop.
desktop_active=$(bspc query -D -d --names)


# if desktop_active is identical with vifm_desktop, then we return to previous
if [ "$desktop_active" = "$vifm_desktop" ]; then
  bspc desktop --focus last
  exit 0
fi




# since we are not yet exit'ed, we can check if vifm is running.
program_node=$(xwininfo -root -tree | awk '/VIFM/ {print $1}')




if [ -n "$program_node" ]; then
  bspc desktop --focus $vifm_desktop
  exit 0
else
  # seems it is time to actually start ranger. first some definitions:
  terminal="/usr/bin/alacritty"
  runfile="/home/jan/.config/vifm/scripts/vifmrun"
  node_name="VIFM"
  node_class="VIFM"

  bspc desktop --focus "$vifm_desktop" && \
    $terminal --title $node_name \
    --class $node_class \
    --command $runfile &
      # vifm is started with title (name) and class for terminal
  exit 0
fi
