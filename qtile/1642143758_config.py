# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# 
# WorkInProgress by jan 2021

import os
import subprocess
##
from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
##
from libqtile import hook

##
@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~/.config/qtile/scripts/autostart.sh')
    subprocess.call([home])

alt = "mod1"
mod = "mod4"
# terminal = guess_terminal()
terminal = "alacritty"
xterminal = "urxvt"
qbin = "/home/jan/.local/qbin/"

keys = [
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    # Key([mod], "a", lazy.spawn('xxx')),
    # Key([mod], "b", lazy.spawn('xxx')),
    # Key([mod], "c", lazy.spawn('xxx')),
    Key([mod], "d", lazy.spawn('rofi -show run')),
    # Key([mod], "e", lazy.spawn('xxx')),
    # Key([mod], "f", lazy.spawn('xxx')),
    # Key([mod], "g", lazy.spawn('xxx')),
    # Key([mod], "i", lazy.spawn('xxx')),
    Key([mod], "m", lazy.spawn(qbin+'dunst_starter.sh'), desc="kills and restart dunst"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Key([mod], "o", lazy.spawn('xxx')),
    Key([mod], "p", lazy.spawn(qbin+'TOGGLE picom'), desc="Toggle picom"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    # Key([mod], "s", lazy.spawn('xxx')),
    # Key([mod], "t", lazy.spawn('xxx')),
    # Key([mod], "u", lazy.spawn('xxx')),
    # Key([mod], "v", lazy.spawn('xxx')),
    # Key([mod], "w", lazy.spawn('xxx')),
    # Key([mod], "x", lazy.spawn('xxx')),
    # Key([mod], "y", lazy.spawn('xxx')),
    # Key([mod], "z", lazy.spawn('xxx')),

    Key([mod], "Left", lazy.screen.prev_group(skip_empty=True)),
    Key([mod], "Right", lazy.screen.next_group(skip_empty=True)),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "space", lazy.screen.toggle_group()),
    Key([mod], "Print", lazy.spawn(qbin+'screenshot.sh'), desc="PRT SC screenshot"),
    Key([mod], "KP_Enter", lazy.spawn(qbin+'RUN speedcrunch'), desc="Calculator"),
    Key([mod], "KP_Add", lazy.spawn(qbin+'RUN bitwarden-desktop'), lazy.group[("2")].toscreen(), desc="Passwordmanager Bitwarden"),
    # Key([mod], "Scroll_Lock", lazy.spawn(qbin+'xxx'), desc="SCR LK"),
    # Key([mod], "Pause", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "Insert", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "Home", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "Prior", lazy.spawn(qbin+'xxx'), desc="PGUP"),
    # Key([mod], "Delete", lazy.spawn(qbin+'xxx'), desc="DEL"),
    # Key([mod], "End", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "Next", lazy.spawn(qbin+'xxx'), desc="PGDN"),
    # Key([mod], "Num_Lock", lazy.spawn(qbin+'xxx'), desc="NUMLK"),
    # Key([mod], "KP_Divide", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "KP_Multiply", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "KP_Subtract", lazy.spawn(qbin+'xxx'), desc=""),
    # Key([mod], "KP_Home", lazy.spawn(qbin+'xxx'), desc=""),

    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    Key([mod, "shift"], "a", lazy.spawn('virtualbox'), lazy.group[("8")].toscreen()),
    Key([mod, "shift"], "b", lazy.spawn(qbin+'RUN brave 2'), lazy.group[("2")].toscreen(), desc="webbrowser"),
    Key([mod, "shift"], "c", lazy.spawn(qbin+'RUN chromium 2'), lazy.group[("2")].toscreen(), desc="webbrowser"),
    Key([mod, "shift"], "d", lazy.spawn(qbin+'RUN darktable 6'), lazy.group[("6")].toscreen(), desc="darktable gfx"),
    # Key([mod, "shift"], "e", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen(), desc=""),
    Key([mod, "shift"], "f", lazy.spawn(qbin+'RUN firefox 2'), lazy.group[("2")].toscreen(), desc="webbrowser"),
    Key([mod, "shift"], "g", lazy.spawn(qbin+'RUN gimp 5'), lazy.group[("5")].toscreen(), desc="GIMP gfx"),
    Key([mod, "shift"], "i", lazy.spawn(qbin+'RUN inkscape 6'), lazy.group[("6")].toscreen(), desc="inkscape gfx"),
    Key([mod, "shift"], "m", lazy.spawn(qbin+'RUN thunderbird 0'), lazy.group[("0")].toscreen(), desc="e-mail epost"),
    # Key([mod, "shift"], "n", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    Key([mod, "shift"], "o", lazy.spawn(qbin+'RUN loffice 5'), lazy.group[("5")].toscreen(), desc="Libre Office Suite"),
    # Key([mod, "shift"], "p", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen(), desc=""),
    Key([mod, "shift"], "q", lazy.spawn(qbin+'RUN qutebrowser 2'), lazy.group[("2")].toscreen(), desc="webbrowser"),
    # Key([mod, "shift"], "r", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "s", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "t", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "u", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    Key([mod, "shift"], "v", lazy.spawn(qbin+'RUN vuescan 6'), lazy.group[("6")].toscreen(), desc="Scanner"),
    # Key([mod, "shift"], "w", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen(), desc=""),
    # Key([mod, "shift"], "x", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "y", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "z", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),

    Key([mod, "shift"], "Return", lazy.spawn(xterminal), desc="Launch terminal"),
    Key([mod, "shift"], "Tab", lazy.prev_layout(), desc="Toggle back layouts"),
    Key([mod, "shift"], "space", lazy.spawn(qbin+'URUN ranger 9 RANGER'), lazy.group[("9")].toscreen()),
    Key([mod, "shift"], "Left", lazy.screen.toggle_group()),
    Key([mod, "shift"], "Right", lazy.screen.next_group(skip_empty=False)),
    Key([mod, "shift"], "Print", lazy.spawn(qbin+'screenshot.sh'), desc="PRT SC Screenshot and preview"),
    # Key([mod, "shift"], "Up", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "Down", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "z", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),
    # Key([mod, "shift"], "z", lazy.spawn(qbin+'xxx'), lazy.group[("1")].toscreen()),


    # resize 
    Key([mod, "control"], "l", lazy.layout.grow_right(), lazy.layout.grow(), lazy.layout.increase_ratio(), lazy.layout.delete()),
    Key([mod, "control"], "h", lazy.layout.grow_left(), lazy.layout.shrink(), lazy.layout.decrease_ratio(), lazy.layout.add()),
    Key([mod, "control"], "k", lazy.layout.grow_up(), lazy.layout.grow(), lazy.layout.decrease_nmaster()),
    Key([mod, "control"], "j", lazy.layout.grow_down(), lazy.layout.shrink(), lazy.layout.increase_nmaster()),
    
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "Return", lazy.layout.toggle_split(), desc="Toggle between split and unsplit sides of stack"),
    
    ]

##################################################################################
    # Key([mod, alt], "h", lazy.layout.swap_left()),
    # Key([mod, alt], "l", lazy.layout.swap_right()),
    # Key([mod, alt], "j", lazy.layout.shuffle_down()),
    # Key([mod, alt], "k", lazy.layout.shuffle_up()),
    # Key([mod, alt], "i", lazy.layout.grow()),
    # Key([mod, alt], "o", lazy.layout.shrink()),
    # Key([mod, alt], "p", lazy.layout.normalize()),
    # Key([mod, alt], "u", lazy.layout.maximize()),
    # Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    # Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    # Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),

    # modadtall/modadwide grow and shrink:  mod+control l,h,k,j
##################################################################################

    
###########################################################################################################################
#
# 
# Group are named, matches kan be added, layout for start can be added
groups = [
        Group("1"),
        Group("2", matches=[Match(wm_class=["Navigator","qutebrowser","brave","chromium","bitwarden"])], layout="treetab"),
        Group("3"),
        Group("4"),
        Group("5"),
        Group("6"),
        Group("7"),
        Group("8", matches=[Match(wm_class=["VirtualBox Manager"])], layout="bsp"),
        Group("9", matches=[Match(title=["ranger"])], layout="bsp"),
        Group("0", matches=[Match(wm_class=["Mail"])], layout="bsp"),
        ]
#
#
# groups
###########################################################################################################################


###########################################################################################################################
#
#
# moving around - move to group (mod+1 etc)
for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(), desc="Switch to group {}".format(i.name)),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True), desc="Switch to & move focused window to group {}".format(i.name)),
    ])
#
#
# moving around
############################################################################################################################


############################################################################################################################
#
#
# layout
def init_layout_theme():
    return {"border_width":3,
            "border_focus": "#99aaaa",
            "border_normal": "#4c5601"
            }

layout_theme = init_layout_theme()

layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(),
    layout.TreeTab(),
    layout.Bsp(**layout_theme),
    # layout.Floating(),
    # layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], border_width=4),
    # layout.Matrix(),
    # layout.RatioTile(),
    # layout.Slice(),
    # layout.Stack(num_stacks=2),
    # layout.Tile(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]
#
#
# layouts
############################################################################################################################


############################################################################################################################
#
#
# widgets
widget_defaults = dict(font='sans', fontsize=12, padding=5,)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.GroupBox(),
                widget.Sep(),
                widget.CurrentLayout(),
                widget.Sep(),
                widget.Spacer(),
                widget.Notify(),
                widget.Chord(chords_colors={'launch': ("#ff0000", "#ffffff"),},name_transform=lambda name: name.upper(),),
                widget.Prompt(),
                # widget.WidgetBox(text_closed = 'More', text_open = '[Less] ', widgets= [
                    # ]),
                # widget.Volume(),
                # widget.Systray(),
                widget.ThermalSensor(),
                widget.Sep(),
                widget.Memory(measure_mem = 'G'),
                widget.HDDBusyGraph(),
                widget.Spacer(),
                widget.DF(warn_space = 6),      ## warn if / is getting full
                widget.Cmus(),
                widget.Volume(),
                widget.CapsNumLockIndicator(),
                widget.Sep(),
                widget.Clock(format='%Y-%m-%d %a %H:%M '),
                widget.Sep(),
                # widget.WindowName(),
                # widget.TextBox("default config", name="default"),
                # widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                # widget.QuickExit(),
            ],
            24,
        ),
    ),
]
#
#
# widgets
############################################################################################################################



# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False




############################################################################################################################
#
#
# Floating rules
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='VirtualBox Manager'),  # gitk
    Match(wm_class='speedcrunch'),  # gitk
    Match(wm_class='bitwarden'),  # gitk
    # Match(wm_class='maketag'),  # gitk
    # Match(wm_class='ssh-askpass'),  # ssh-askpass
    # Match(title='branchdialog'),  # gitk
    # Match(title='pinentry'),  # GPG key password entry
])
#
#
# Floating rules
############################################################################################################################


auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.


wmname = "LG3D"

