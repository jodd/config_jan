#!/usr/bin/sh

external_ip_now=$(curl ifconfig.me/ip)
external_ip_last=$(tail -1 ~/tmp/external_ip_log)

# external_ip_last=$(sed '1q;p' ~/tmp/external_ip_log)

if [ "$external_ip_now" = "$external_ip_last" ]; then
  notify-send "IP external address checked"
else
  # ip has changed (or is not available)
  #
  # add date for this record
  to_day_date=$(date)
  printf "$to_day_date\n" >> ~/tmp/external_ip_log
  printf "$external_ip_now\n" >> ~/tmp/external_ip_log
  notify-send -u critical "IP Changed" $external_ip_now
fi

