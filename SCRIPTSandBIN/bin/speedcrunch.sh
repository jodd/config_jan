#!/usr/bin/bash


node_id_speedcrunch=$(xwininfo -root -tree | awk '/speedcrunch/ { a = $1 } END { print a}')


if [ -n "$node_id_speedcrunch" ]; then
  bspc node $node_id_speedcrunch --flag hidden --focus
else
  /usr/bin/speedcrunch &
fi
