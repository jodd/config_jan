let VIMRUNTIME = "/home/jan/.config/nvim"

let mapleader = ","

"if problem, deactivate by removing comment
"let loaded_netrwPlugin = 1
"let did_load_ftplugin = 1
"let loaded_vimwiki = 1



"color
colorscheme mdelek
" colorscheme wal

filetype plugin indent on
syntax on

autocmd FileType xdefaults set commentstring=!\ %s
":e /usr/share/nvim/runtime/filetype.vim
"It doesn't seeems to work, must be set locally when editing: :set filetypecommnet=!\ %s
autocmd BufRead,BufNewFile *.tex,*.txt set filetype=tex

" autocmd FileType tex setlocal spell spellang=en_US

"Senter when insert
" autocmd InsertEnter * norm zz

"Remove trailing whitespace on saving
autocmd BufWritePre * %s/+s++$//e

" Pluginstall, plug.vim
" mkdir -p ~/.local/share/nvim/site/autoload
" cd ~/.local/share/nvim/site/autoload
" need: curl
" curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -o
" plug.vim
"
" commands plug.vim:
" :PlugUpdate
" :PlugClean
" :PlugStatus
" :PlugInstall vim-css-color
"
call plug#begin('~/.local/share/plugvims')

Plug 'chrisbra/Colorizer'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'vimwiki/vimwiki'
Plug 'junegunn/goyo.vim'
" Plug 'dylanaraps/wal.vim'

call plug#end()



"vimwiki makes and reads .md instead of .wiki
let g:vimwiki_list = [{'path': '~/Public/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]

"vim-surround
"   ds"      remove    "test"  test
"   cs]{    makes   [test]  to:   { test }  
"   cs]}    makes   [test]  to:   {test}
"   cs]'    makes   [test]  to:   'test'
"   cs])    makes   [test]  to:   (test)
"   cs](    makes   [test]  to:   ( test )
"   cs]"    makes   [test]  to:   "test"
"   cs]'    makes   [test]  to:   'test'
"   cs]=    makes   [test]  to:   =test=
"

"MAPPINGS
"remove unneeded bindings:
no <down> <Nop>
no <left> <Nop>
no <right> <Nop>
no <up> <Nop>
ino <down> <Nop>
ino <left> <Nop>
ino <right> <Nop>
ino <up> <Nop>


"Some shorts I use all the time
ino jj <esc>
ino JJ <esc>A

vmap JJ <esc>

"testing some shorts:
imap HH <esc>I
noremap <silent> <C-k> 10k
noremap <silent> <C-j> 10j


"Jumping buffers
nnoremap <Tab> :bnext<CR>
nnoremap <S-Tab> :bprev<CR>



"Clear search highlight
 nnoremap <C-l> :nohl<CR><C-L>

 "Colorize Plugin
map <leader>c   :ColorHighlight<CR>
map <leader>C   :ColorClear<CR>


"autoindent on/off
map <leader>i   :setlocal autoindent<CR>
map <leader>I   :setlocal noautoindent<CR>

"center document when entering instert mode
"utocmd InsertEnter * norm zz



"Tabs
"Open new tab
map <leader>tn :tabnew<CR>

"Close tab
map <leader>tc :tabclose

"open yanked file from standard reg (*) in ranger: <selected file> yp
map <leader>tp :tabnew <C-r>*<CR>



"sensible split behavour
set splitbelow splitright

"shortcuts for split navigation
nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

nnoremap <silent> <A-Left> :vertical resize +3<CR>
nnoremap <silent> <A-Right> :vertical resize +3<CR>
nnoremap <silent> <A-Up> :resize +3<CR>
nnoremap <silent> <A-Down> :resize +3<CR>

"split
nnoremap <leader>h :split<CR>
nnoremap <leader>v :vsplit<CR>

" delete split
nnoremap <leader>d :only<CR>

"visual tabindent
vmap < <gv
vmap > >gv

"dd/D   yy/Y
nmap Y y$

"set stuff
" =============================================================================
" set nobackup      "no backups
" set noswapfile    "no swap
" set t_Co=256      "set if term supports 256 colors
" set noshowmode    "???
set nocompatible
set encoding=utf-8

"using system clipboard, p paste from commandline or stuff without :reg
" set clipboard=unnamedplus

set hidden                          "Make it possible to <Tab> thru buffers without save changes
set wildmenu                        "Tab-completion of commands in vim
set showcmd                         "Shows last used command at cmd-line
set showmatch                       "Find matching bracket, jump with %
set hlsearch                        "Highlight search result
set incsearch                       "Highlight as it is written
set ignorecase                      "BIG or small letters automatically
set smartcase                       "ignore case if search pattern is lower
set path+=**                        "Searches recursively

" Cleans up hassle if accidentally hitting something, don't rember what
set backspace=indent,eol,start

" Linenumbering, red line at the 79 length short
set number
set relativenumber
set cursorline
" set cursorculomn
" set colorcolumn=79

set shiftwidth=2                    "Tablengt
set softtabstop=2
set expandtab                       "Change tab to the set number of spaces
set autoindent                      "autoindent on/off: i/I
set nostartofline                   "Put cursor at first blank, start of line, not at at set point

set mouse=a                         "Enables mouse at any modes:  https://vim.fandom.com/wiki/Example_vimrc

" keeps insertpoint in the middele of screen if possible: Nice!
set sidescroll=999
set scrolloff=999

"Not needed b/c statusline
"set ruler

set laststatus=2                    "Statusline

"Defining statusline
set statusline=
set statusline+=%#DiffDelete#
set statusline+=\[%n]
set statusline+=\ %F
set statusline+=\ %r
set statusline+=%=
set statusline+=\ %y
set statusline+=\ <%{&fileencoding?&fileencoding:&encoding}>
set statusline+=%#PmenuSel# 
set statusline+=\[%5l]:
set statusline+=\[%5c]
set statusline+=\ %5L
set statusline+=\ %3p%%
set statusline+=\ -
"   --- /http://vimdoc.sourceforge.net/htmldoc/options.html#'statusline
