#!/usr/bin/env bash

# Backup for this software

# shotwell
# keepassx2
# i3
# vim
# oh-my-zsh
# .Xdefaults
# Thunderbird


# logfil i ~
LOG="/home/jan/.backup-log"
# Year-Month-Date
DAT=$(date +%Y-%m-%d)
# Hour:Minutes:Secounds
TID=$(date +%T)
echo "*************************************************" >> $LOG
echo "backup" >> $LOG
echo "$DAT $TID" >> $LOG
echo "*************************************************" >> $LOG
echo "*************************************************" >> $LOG


# Server og PATH til backup dir...
BACKUP="jan@mink:/home/jan/TERRAID/backjan"


# --recursive --archive --verbose --update --links -e ssh

# Shotwell i saved in two folders: ~/.cache/shotwell & ~/.local/share/shotwell
#echo "~/.cache/shotwell/" >> $LOG
#rsync -ravul -e ssh ~/.cache/shotwell/ $BACKUP/.cache/shotwell >> $LOG
#echo "~/.local/share/shotwell/" >> $LOG
#rsync -ravul -e ssh ~/.local/share/shotwell/ $BACKUP/.local/share/shotwell >> $LOG
# server:TERRAID/backjan/.cache/shotwell
# server:TERRAID/backjan/.local/share/shotwell


# keepassx is saved in one folder
echo "~/.config/keepassx/" >> $LOG
rsync -ravul -e ssh ~/.config/keepassx/ $BACKUP/.config/keepassx >> $LOG
## Denne virker, trenger ->
# server:TERRAID/backjan/.config
# server:TERRAID/backjan/.config/keepassx

echo "~/.i3/" >> $LOG
rsync -ravul -e ssh ~/.i3/ $BACKUP/.i3 >> $LOG
echo "~/.config/i3status/" >> $LOG
rsync -ravul -e ssh ~/.config/i3status/ $BACKUP/.config/i3status >> $LOG
## Denne virker, trenger ->
# server:TERRAID/.i3
# server:TERRAID/.config/i3status

echo "~/.vim/" >> $LOG
rsync -ravul -e ssh ~/.vim/ $BACKUP/.vim >> $LOG
echo "~/.vimrc" >> $LOG
rsync -avul -e ssh ~/.vimrc $BACKUP >> $LOG
## Denne virker, trenger ->
# server:TERRAID/.vim


echo "~/.Xdefaults" >> $LOG
rsync -avul -e ssh ~/.Xdefaults $BACKUP >> $LOG
## OK

echo "~/.zprezto" >> $LOG
rsync -ravul -e ssh ~/.zprezto $BACKUP >> $LOG
# server:TERRAID/.zprezto

echo "/etc" >> $LOG
rsync -ravul -e ssh /etc \
	--exclude 'ssh' \
	--exclude 'gshadow' \
	--exclude 'gshadow-' \
	--exclude 'shadow' \
	--exclude 'shadow-' \
	--exclude 'sudoers' \
	--exclude 'sudoers.d' \
	--exclude 'cups' \
	--exclude 'pacman.d' \
	--exclude 'polkit-1' \
	--exclude '.pwd.lock' \
	--exclude 'crypttab' \
	--exclude 'lvm' \
	--exclude 'NetworkManager' \
	--exclude 'audisp' \
	--exclude 'libaudit.conf' \
	$BACKUP >> $LOG
# server:TERRAID/etc

#echo "~/.zshrc" >> $LOG
#rsync -avul -e ssh ~/.zshrc $BACKUP >> $LOG
##echo "~/.oh-my-zsh/" >> $LOG
##rsync -ravul -e ssh ~/.oh-my-zsh/ $BACKUP/.oh-my-zsh >> $LOG
## Denne virker, trenger ->

echo "~/.thunderbird/" >> $LOG
rsync -ravul -e ssh ~/.thunderbird/ $BACKUP/.thunderbird >> $LOG
## Virker, trenger ->
# server:TERRAID/.thunderbird

echo "/home/DATA/Documents/ -> Documents-varig" >> $LOG
rsync -ravul -e ssh /home/DATA/Documents/ \
	--exclude 'inga' \
	--exclude 'pdf' \
	--exclude 'manualer' \
	$BACKUP/Documents-varig >> $LOG
## Denne virker, SLETTER INGENTING, Trenger ->
# server:TERRAID/Documents-varig

echo "/home/DATA/Documents" >> $LOG
rsync --delete -ravul -e ssh /home/DATA/Documents/ \
	--exclude 'inga' \
	--exclude 'pdf' \
	--exclude 'manualer' \
	$BACKUP/Documents >> $LOG
## Denne virker, SLETTER FILER SOM MANGLER, Trenger ->
# server:TERRAID/Documents

echo "/home/DATA/bash/ >bash " >> $LOG
rsync -ravul -e ssh /home/DATA/bash/ $BACKUP/bash >> $LOG
## Denne virker, SLETTER INGENTING, Trenger ->
# server:TERRAID/bash

echo "/home/DATA/Pictures" >> $LOG
rsync -ravul -e ssh /home/DATA/Pictures/ $BACKUP/Pictures >> $LOG
##
# server:TERRAID/Pictures

echo "/var/cache/pacman/pkg" >> $LOG
rsync -ravul -e ssh /var/cache/pacman/pkg $BACKUP/var/cache/pacman/pkg >> $LOG
##
# server:TERRAID/backjan/var/cache/pacman/pkg

TIDF=$(date +%H:%M:%S)
echo "-----------------------------------------" >> $LOG
echo $TID >> $LOG
echo $TIDF  >> $LOG
echo "Time/Time used Last Backup" >> $LOG
echo "-----------------------------------------" >> $LOG

