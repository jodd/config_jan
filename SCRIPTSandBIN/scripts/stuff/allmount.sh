#!/usr/bin/bash

# list all files in folder ~/S and mounts them if they is not "usb"

for d in $(ls ~/S)
do
	[[ $d != "usb" ]] && sudo mount /dev/$d ~/S/$d
done
