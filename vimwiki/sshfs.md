#   sshfs and fusermount

sshfs jan@mira:/home/jan/ mira-home/
fusermount -u mira-home		#Cannot use umount

 fusermount3: needs to be installed setuid 'root'
 As root, run the following command to ensure that the permissions of the fusermount3 file are -rwsr-xr-x

sudo chmod 4755 /usr/bin/fusermount3
