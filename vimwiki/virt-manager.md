https://virt-manager.org/

About virt-manager’s supporting tools
virt-install is a command line tool which provides an easy way to provision operating systems into virtual machines.

virt-viewer is a lightweight UI interface for interacting with the graphical display of virtualized guest OS. It can display VNC or SPICE, and uses libvirt to lookup the graphical connection details.

virt-clone is a command line tool for cloning existing inactive guests. It copies the disk images, and defines a config with new name, UUID and MAC address pointing to the copied disks.

virt-xml is a command line tool for easily editing libvirt domain XML using virt-install’s command line options.

virt-bootstrap is a command line tool providing an easy way to setup the root file system for libvirt-based containers.

QEMU
KVM

sudo pacman virt-manager qemu qemu-arch-extra ovmf vde2 ebtables dnsmasq bridge-utils openbsd-netcat

# NB
  activate in BIOS: Virtualizations
virt-manager            # front end gui for kvm
quemu                   # basic pacage
quemu-arch
ovmf                    # for UEFI installations
vde2                    # networking tool
ebtables                # network package
dnsmasq                 # network package
bridge-utils
openbsd-netcat          

sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service

# New bridge
sudo nvim br10.xml
<network>
  <name>br10</name>
  <forward mode='nat'>
    <nat>
      <port start='1024' end='65535'/>
    </nat>
  </forward>
  <bridge name='br10' stp='on' delay='0'/>
  <ip address='192.168.30.1' netmask='255.255.255,0'>
    <dhcp>
      <ranger start='192.168.30.50' end='192.168.30.200'/>
    </dhcp>
  </ip>
</network>
https://gitlab.com/eflinux/kvmarch/-/blob/master/br10.xml

sudo virsh net-define br10.xml

sudo virsh net-start br10

sudo virsh net-autostart br10

# ----
# QEMU/KVM for absolute beginners 
# Veronica Explains:
egrep -c '(vmx|svm)' /proc/cpuinfo
  if greater than 0, good to go
  
qemu is the emulator itself, software
libvirt runs in background
bridge-utils: importannt networking dependecies
virt-manager: the graphical program wwe'll use to work with our WMs

probably:
sudo usermod -aG libvirt jan
sudo usermod -aG kwm jan

sudo systemctl enable libvirtd
sudo systemctl start libvirtd

virtual-manager:  graphical
virsh:            cli
https://computingforgeeks.com/virsh-commands-cheatsheet/



