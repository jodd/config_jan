#!/usr/bin/sh


# Server og PATH til backup dir...
BACKUP="jan@mink:/home/jan/TRETERRAID/backup-min/"

# logfil i ~
LOG="/home/jan/.backup-log.txt"

TimeDate=$(date -Is | awk -F+ '{ printf $1 }')
LINEOFDASH="---------------------------------------------------------------"

printf "%60s\n" $LINEOFDASH "Backup: $TimeDate" " " $LINEOFDASH >> $LOG



# Year-Month-Date
# DAT=$(date +%Y-%m-%d)
# Hour:Minutes:Secounds
# TID=$(date +%T)

# printf "backup: $DAT $TID \n^^^^^^^^^^^^^^^^^^^\n" >> $LOG
# printf "backup: $DAT $TID \n\\\/\\\/\\\/\\\/\\\/\\\/\\\/\\\/\\\/\\\/\n" >> $LOG


# printf "~/.config/{bspwm,sxhkd,polybar,nvim,ranger,zsh}\n" >> $LOG
echo ".config: bspwm, sxhkd, polybar, nvim, ranger, zsh" >> $LOG
rsync -ravul -e ssh ~/.config/{bspwm,sxhkd,polybar,nvim,ranger,zsh} $BACKUP

# printf "~/Documents/{privat,prosjekt,lx}\n" >> $LOG
echo "Documents: privat, prosjekt, lx" >> $LOG
rsync -ravul -e ssh ~/Documents/{privat,prosjekt,lx} $BACKUP

# printf "~/vimwiki\n" >> $LOG
echo "vimwiki" >> $LOG
rsync -ravul -e ssh ~/vimwiki $BACKUP

# printf "~/.local/{scripts,bin}\n" >> $LOG
echo ".local: scripts, bin" >> $LOG
rsync -ravul -e ssh ~/.local/{scripts,bin} $BACKUP

# printf "~/.local/share/thunderbird\n" >> $LOG
echo "Thunderbird" >> $LOG
rsync -ravul -e ssh ~/.local/share/thunderbird $BACKUP

# printf "/var/cache/pacman/pkg\n" >> $LOG
echo "/var/cache/pacman/pkg" >> $LOG
rsync -ravul --delete -e ssh /var/cache/pacman/pkg $BACKUP

TID=$(date +%T)
printf "%60s\n" $LINEOFDASH $LINEOFDASH "Finished $TID" >> $LOG
