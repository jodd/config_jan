#!/usr/bin/sh

## bspc query -N -n .hidden
## bspc node --flag hidden
## bspc node 0x03C00003 --flag hidden     >> then it will be shown

# when starting galculator: bspc query -N -n focused
#                           will show node ID
# before this i sleep for 1 sec, don't know if it is needed, but does'n matter.
# galculator is running and ready for action...

galculator_pid=$(pidof galculator)
if [ -n "$galculator_pid" ]; then

  galculator_node=$(cat /tmp/galculator_node)
  bspc node $galculator_node --flag hidden --focus
  exit 0
else
  if [ -a /tmp/galculator_node ]; then
    /usr/bin/rm /tmp/galculator_node
  fi
  /usr/bin/galculator &
  sleep 1
  galculator_node=$(bspc query -N -n focused)
  printf "$galculator_node" > /tmp/galculator_node

  exit 0
fi

