#!/usr/bin/bash

# change wallpaper 
# if file "$FILE" |grep -qE 'image|bitmap'; then
#   echo "File '$FILE' has the headers of an image"
# fi

wallpaperdir='/home/jan/.config/wallpaper'


# if file "$1" | grep 'image'; then
#     echo "OK"
# elif
#     echo "Needs an image file " $1 " is maybe not an image!"
#     exit 1
# fi

[ -L "$wallpaperdir"/current_wallpaper ] && \
    rm "$wallpaperdir"/current_wallpaper
# if the link exist, then delete it..

ln -s $1 /home/jan/.config/wallpaper/current_wallpaper
feh --bg-fill "$wallpaperdir"/current_wallpaper &



