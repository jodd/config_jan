#!/usr/bin/bash


active=$(xwininfo -tree -root | grep "RANGER")


echo "$active"

if [[ $active ]]; then
  qtile cmd-obj -o group "9" -f 'toscreen'
else
  alacritty --title "RANGER" -e /usr/bin/ranger
fi
  alacritty --title "RANGER" -e /usr/bin/ranger
