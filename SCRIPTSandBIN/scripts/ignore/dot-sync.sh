#!/bin/bash

# Backing up a lot of dot-files, nice to make fresh when moving to another
# system and general backup or cloning systems

#rsync --recursive --archive --verbose --update --append ~/ --exclude="Desktop" --exclude="Documents/" --exclude="Downloads" --exclude="Pictures" --exclude="Videos" --exclude="mink" --exclude="tmp" --exclude="lost+found" --exclude=".bash_history" --exclude=".cache" --exclude=".gimp*" --exclude=".gnome" --exclude=".kde*" --exclude=".local" --exclude=".mono" --exclude=".mozilla" --exclude=".nv" --exclude=".pki" --exclude=".recently-used" --exclude=".thunderbird" /home/jan/mink/TRETERRAID/dots/ 

# rsync --dry-run --recursive --archive --verbose --update --append ~/ --exclude="Desktop" --exclude="Documents/" --exclude="Downloads" --exclude="Pictures" --exclude="Videos" --exclude="mink" --exclude="tmp" --exclude="lost+found" --exclude=".bash_history" --exclude=".cache" --exclude=".gimp*" --exclude=".gnome" --exclude=".kde*" --exclude=".local" --exclude=".mono" --exclude=".mozilla" --exclude=".nv" --exclude=".pki" --exclude=".recently-used" --exclude=".thunderbird" /home/jan/mink/TRETERRAID/dots/ 
 

# rsync --dry-run --recursive --archive --verbose --update --append -e ssh ~/ --exclude="Desktop" --exclude="Documents/" --exclude="Downloads" --exclude="Pictures" --exclude="Videos" --exclude="mink" --exclude="tmp" --exclude="lost+found" --exclude=".bash_history" --exclude=".cache" --exclude=".gimp*" --exclude=".gnome" --exclude=".kde*" --exclude=".local" --exclude=".mono" --exclude=".mozilla" --exclude=".nv" --exclude=".pki" --exclude=".recently-used" --exclude=".thunderbird" jan@mink:/home/jan/TRETERRAID/dots/  

rsync\
	--dry-run\
	--recursive\
	--archive\
	--verbose\
	--update\
	--append\
	-e ssh ~/\
	--exclude=VM\
	--exclude="Desktop"\
	--exclude="Documents/"\
    --exclude="Downloads"\
	--exclude="Pictures"\
	--exclude="Videos"\
	--exclude="mink"\
	--exclude="tmp"\
	--exclude="lost+found"\
	--exclude=".bash_history"\
	--exclude=".cache"\
	--exclude=".gimp*"\
	--exclude=".gnome"\
	--exclude=".kde*"\
	--exclude=".local"\
	--exclude=".mono"\
	--exclude=".mozilla"\
	--exclude=".nv"\
	--exclude=".pki"\
	--exclude=".recently-used"\
	--exclude=".thunderbird"\
	jan@mink:/home/jan/TRETERRAID/dots/  
echo "remove --dry-run"
