#!/usr/bin/bash

current_external_ip=$(curl ifconfig.me/ip)

current_local_ip=$(ip route | tail -1 | cut -d' ' -f9)
# current_external_ip=$(ip route | awk -F/ 'NR==2 {print $1}')
# current_external_ip=$(ip route | awk 'NR==2 {print $1}')
#

notify-send 'IP: Local / External:' "$current_local_ip\n$current_external_ip"
