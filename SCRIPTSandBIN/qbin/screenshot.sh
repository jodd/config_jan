#!/usr/bin/sh

# scrot makes a screenshot
## --silent, -z
## --delay NUM, -d NUM	NUM=sekunder
## --exec APP, -e APP	-e feh åpner screenshot med feh
## --select, -s 		interaktiv
## %s is unix time
f_name="snap_"$(date +%s%N)".png"
d_name="$HOME"/Pictures/screenshots/

/usr/bin/scrot "$d_name$f_name"
pgrep -x dunst && notify-send -t 800 "saved:" "$d_name$f_name"

