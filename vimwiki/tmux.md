
# mink start tmux with some nice layouts

tmux new-session -t top
top
C+b %
C+b :resize-pane -R 20
watch -n 3 sensors
C+b d

tmux new-session -t bash
C+-b %
C+b d

