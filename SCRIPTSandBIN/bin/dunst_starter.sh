#!/usr/bin/bash

# if dunst is running, kill it
pgrep -x dunst >/dev/null && killall dunst

# sleep until dunst is completely killed
while pgrep -x dunst
do
    sleep 1
done

/usr/bin/dunst &

sleep 2
notify-send "DUNST" "...running now!"
