#!/usr/bin/bash

# script to easy install plug.vim for nvim

# cd ~/.config
# git clone https://gitlab.com/jodd/nvim.git 

mkdir -p ~/.local/share/plugvims
mkdir -p ~/.local/share/nvim/site/autoload/
cd ~/.local/share/nvim/site/autoload/
curl https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim -o plug.vim

