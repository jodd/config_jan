#!/usr/bin/sh

# click(right button) to toggle sticky+floating and tiled

node_id=$(bspc query -n focused -N)
bspc node $node_id --state floating --flag sticky || bspc node $node_id --state tiled --flag sticky=off
printf "$node_id\n"
