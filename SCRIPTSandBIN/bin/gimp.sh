#!/usr/bin/sh

# for some reason, I've had problems running gimp witout darktable
# when starting gimp, it's not completely started and halts, if darktable also must start. darktabel must be sent to background or quitted for gimp to start

# I check if darktable is running, if not I start it.
# then I start gimp

# pid_darktable=$(pidof darktable)

# [ -z "$pid_darktable" ] || "$

# pidof darktable || "$HOME"/.local/bin/darktable.sh &
pidof gimp || /usr/bin/gimp &
bspc desktop 5 --focus
exit 0

# TODO
# I have remove to files from /usr/lib/gegl-0.4/
# exr-load.so
# exr-save.so
# saved in ~/.config/GIMP/
# found something online:
# sudo apt remove gimp
# sudo apt remove *gegl* # to remove all gegl libraries. Then,
# sudo apt install gimp # to reinstall Gimp. The gegl libraries are reinstalled here too.
